<?php

ini_set('display_errors', 'On');
error_reporting(E_ALL & ~E_NOTICE);

$uri = $_SERVER['REQUEST_URI'];
$uri_arr = explode('/', $uri);

switch( $uri_arr[1] ){

    case 'cache':
        $time = $uri_arr[2];
        $link = $_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.substr($uri, strpos($uri, '/cache/') + 7 + strlen($time) + 1 );
        return the_cache($time, $link);

    default:
        echo 'no task to do';

}


function the_cache( $time, $url ){

    $md5 = md5($url.'-'.$time);
    $path = take_care_of_dir("../tmp/".substr($md5, 0, 4)."/" );
    $file = $path.$md5;

    $h = get_headers($url, true);
    header("Content-Type: ".$h['content-type']);

    if( file_exists($file) ){

        $time_trailer = strtolower(substr($time,-1));
        
        switch( $time_trailer ){
            case 's':
                $time = substr($time, 0, -1);
                break;
            case 'm':
                $time = substr($time, 0, -1) * 60;
                break;
            case 'h':
                $time = substr($time, 0, -1) * 3600;
                break;
            case 'd':
                $time = substr($time, 0, -1) * 3600 * 24;
                break;
            case 'w':
                $time = substr($time, 0, -1) * 3600 * 168;
                break;
        }

        // hit
        if( filemtime($file) + $time > date('U') ){
            readfile($file);
            die;
        }

    }
        
    $content = @file_get_contents($url);
    $code = getHttpCode($http_response_header);
    
    if( $code != 200 ){
        http_response_code($code);
        
    } else {
        file_put_contents($file, $content);
    }

    echo $content;

}

function take_care_of_dir( $dir ){
    
    shell_exec("mkdir -p {$dir}");
    // shell_exec("chmod 0777 {$dir}");
    shell_exec("chown -R www-data.www-data {$dir}");
    
    if(! file_exists($dir) ){
        echo __FUNCTION__." unsuccessful for ".$dir;
        die;
    }

    return $dir;

}


function getHttpCode($http_response_header)
{
    if(is_array($http_response_header))
    {
        $parts=explode(' ',$http_response_header[0]);
        if(count($parts)>1) //HTTP/1.0 <code> <text>
            return intval($parts[1]); //Get code
    }
    return 0;
}

